FROM cytopia/ansible:latest-infra AS getter

RUN mkdir tmp
WORKDIR /tmp
# clone the repo
RUN git clone https://github.com/kubernetes-sigs/kustomize.git

# ---

FROM golang:1.16-alpine AS build

COPY --from=getter /tmp/kustomize /kustomize
WORKDIR /kustomize

RUN apk update && \
    apk add --no-cache gcc g++ musl-dev 

# Need go 1.13 or higher
# see https://golang.org/doc/go1.13#modules
RUN unset GOPATH && unset GO111MODULES && export GOBIN=/tmp && (cd kustomize; go install .)
#COPY ~/go/bin/kustomize /tmp/

# ---

FROM alpine:3.12

COPY --from=build /tmp/kustomize /usr/bin/kustomize

RUN apk update && \
	apk add --no-cache git

ENTRYPOINT [""]